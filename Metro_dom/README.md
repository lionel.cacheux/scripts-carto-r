# Metro_dom
Création d'un fond regroupant les fonds métropole, DROM et un zoom sur Paris et ses départements limitrophes

![](Metro_dom/Rplot_metroDom.png)

## Paramètres des transformations

|| **Zoom** | **Translation horizontale** | **Translation verticale** |
| ------ | ------:| ------:| ------:|
|Corse | 1,00 | -166 400 | 0|
|Guadeloupe | 1,32 | -699 983 | 4 269 050 |
|Martinique | 1,85 | -1 134 525 | 3 517 169 |
|Guyane | 0,25 | 118 687 | 6 286 270 |
|La Réunion | 1,75 | -422 169 | -7 132 230 |
|Mayotte | 2,45 | -1 082 300 | -14 868 225 |
|Zoom Paris | 2,78 | -1 634 506 | -12 046 235 |